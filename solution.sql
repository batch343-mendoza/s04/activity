-- a. All artist that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- b. Songs that is less than 3:50
SELECT * FROM songs WHERE length < 350;

-- c.
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- d.
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE album_title LIKE "%a%";

-- e.
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- f.
SELECT * FROM albums 
	JOIN songs ON albums.id = songs.album_id
	ORDER BY album_title DESC;